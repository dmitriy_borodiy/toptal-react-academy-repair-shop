import PropTypes from 'prop-types';
import { RepairStatus, UserRole } from './constants';

export const HistoryShape = PropTypes.shape({
  push: PropTypes.func.isRequired,
});

export const CommentShape = PropTypes.shape({
  key: PropTypes.string.isRequired,
  userKey: PropTypes.string.isRequired,
  date: PropTypes.number.isRequired,
  text: PropTypes.string.isRequired,
});

export const CommentsShape = PropTypes.arrayOf(CommentShape);

export const RepairStatusShape = PropTypes.oneOf([RepairStatus.COMPLETE, RepairStatus.INCOMPLETE]);

export const RepairShape = PropTypes.shape({
  key: PropTypes.string.isRequired,
  userKey: PropTypes.string,
  time: PropTypes.number.isRequired,
  comments: CommentsShape,
  status: RepairStatusShape.isRequired,
  approved: PropTypes.bool.isRequired,
});

export const RepairsShape = PropTypes.arrayOf(RepairShape);

export const UserRoleShape = PropTypes.oneOf([UserRole.USER, UserRole.MANAGER]);

export const UserShape = PropTypes.shape({
  key: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  role: UserRoleShape,
});

export const UsersShape = PropTypes.arrayOf(UserShape);

export const FiltersShape = PropTypes.shape({
  dateStart: PropTypes.number,
  dateEnd: PropTypes.number,

  hoursStart: PropTypes.number,
  minutesStart: PropTypes.number,
  amPmStart: PropTypes.string,

  hoursEnd: PropTypes.number,
  minutesEnd: PropTypes.number,
  amPmEnd: PropTypes.string,

  userKey: PropTypes.string,
  status: RepairStatusShape,
});
