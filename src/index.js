import React from 'react';
import ReactDOM from 'react-dom';

import 'react-dates/lib/css/_datepicker.css';
import 'semantic-ui-css/semantic.min.css';
import './index.css';

import './polyfills';

import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
