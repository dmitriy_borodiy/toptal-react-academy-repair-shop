import firebase from 'firebase';

const firebaseConfig = {
  apiKey: 'AIzaSyBYBwRuWt1cXgwjKxGtwYJblEeey6MEOew',
  authDomain: 'toptal-react-academy-55d03.firebaseapp.com',
  databaseURL: 'https://toptal-react-academy-55d03.firebaseio.com',
  projectId: 'toptal-react-academy-55d03',
  storageBucket: 'toptal-react-academy-55d03.appspot.com',
  messagingSenderId: '330615928323',
};

firebase.initializeApp(firebaseConfig);

export default firebase;

// Some utility functions

export function subscribeToRefAsList(ref, callback) {
  ref.on('value', (snapshot) => {
    const data = snapshot.val();
    let records = [];
    if (data) {
      const keys = Object.keys(data);
      records = keys.map(key => ({ key, ...data[key] }));
    }
    callback(records);
  });
}

export function subscribeToRefAsObject(ref, callback) {
  ref.on('value', (snapshot) => {
    const data = snapshot.val();
    callback(data);
  });
}

export function unsubscribeFromRef(ref) {
  ref.off('value');
}
