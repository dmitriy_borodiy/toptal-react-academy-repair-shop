import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Form, Header, Message, Segment } from 'semantic-ui-react';

import firebase from '../firebase';
import { CenterFormLayout } from '../Layout';
import './LoginPage.css';

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    const state = {
      submitting: false,
      hasSubmitted: false,
      values: {
        email: null,
        password: null,
        passwordRepeat: null,
      },
      valid: true,
      error: null,
      errors: {},
    };

    const errors = this.validate(state.values);
    this.state = {
      ...state,
      errors,
      valid: Object.keys(errors).length === 0,
    };
  }

  handleFieldChange = (key, value) => {
    const newValues = {
      ...this.state.values,
      [key]: value,
    };

    const validationResult = this.validate(newValues);
    const valid = Object.keys(validationResult).length === 0;

    this.setState({
      values: newValues,
      errors: validationResult || {},
      valid,
      error: null,
    });
  };

  validate = (values) => {
    const errors = {};
    if (!values.email || values.email.length === '') {
      errors.email = 'Is required';
    }
    if (!values.password || values.password.length === '') {
      errors.password = 'Is required';
    }
    return errors;
  };

  handleEmailChange = event => this.handleFieldChange('email', event.target.value);
  handlePasswordChange = event => this.handleFieldChange('password', event.target.value);

  handleSubmit = async () => {
    if (!this.state.valid) {
      this.setState({
        error: null,
        hasSubmitted: true,
      });
      return;
    }

    this.setState({
      hasSubmitted: true,
      submitting: true,
    });

    try {
      const { email, password } = this.state.values;
      await firebase.auth().signInWithEmailAndPassword(email, password);
    } catch (error) {
      console.error(error);
      this.setState({
        error: error.message || 'Error while signing in',
        submitting: false,
      });
    }
  };

  render() {
    return (
      <CenterFormLayout>
        <Header as="h2" color="teal" textAlign="center">
          {' '}
          Log-in to your account
        </Header>
        <Form
          size="large"
          loading={this.state.submitting}
          error={!this.state.valid || !!this.state.error}
        >
          <Segment stacked>
            <Form.Field error={this.state.hasSubmitted && this.state.errors.email}>
              <Form.Input
                id="email"
                fluid
                icon="at"
                iconPosition="left"
                placeholder="E-mail address"
                onChange={this.handleEmailChange}
              />
            </Form.Field>
            {this.state.hasSubmitted &&
              this.state.errors.email && <Message error>{this.state.errors.email}</Message>}

            <Form.Field error={this.state.hasSubmitted && this.state.errors.email}>
              <Form.Input
                fluid
                icon="lock"
                iconPosition="left"
                placeholder="Password"
                type="password"
                onChange={this.handlePasswordChange}
              />
            </Form.Field>
            {this.state.hasSubmitted &&
              this.state.errors.password && <Message error>{this.state.errors.password}</Message>}

            {this.state.error && <Message error>{this.state.error}</Message>}

            <Button color="teal" fluid size="large" onClick={this.handleSubmit}>
              Login
            </Button>
          </Segment>
        </Form>
        <Message>
          New to us? <Link to="/signup">Sign Up</Link>
        </Message>
      </CenterFormLayout>
    );
  }
}

export default LoginForm;
