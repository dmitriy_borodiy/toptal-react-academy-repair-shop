import React from 'react';
import { Dimmer, Loader } from 'semantic-ui-react';

import { HistoryShape } from '../shapes';
import firebase from '../firebase';

class LogoutPage extends React.Component {
  static propTypes = {
    history: HistoryShape.isRequired,
  };

  componentDidMount() {
    firebase
      .auth()
      .signOut()
      .then(() => this.props.history.push('/login'));
  }

  render() {
    return (
      <Dimmer active page>
        <Loader size="massive">Logging Out...</Loader>
      </Dimmer>
    );
  }
}

export default LogoutPage;
