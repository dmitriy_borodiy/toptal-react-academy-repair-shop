import firebase from '../firebase';
import { RepairStatus } from '../constants';

const API_BASE = 'https://us-central1-toptal-react-academy-55d03.cloudfunctions.net';

class DataService {
  updateUser = (id, data) =>
    firebase
      .database()
      .ref(`/users/${id}`)
      .update(data);

  deleteUser = id =>
    firebase
      .database()
      .ref(`/users/${id}`)
      .remove();

  createRepair = data => this.updateRepair(null, data);

  updateRepair = async (id, data) => {
    const key =
      id ||
      firebase
        .database()
        .ref('/repairs')
        .push().key;

    const token = await firebase.auth().currentUser.getIdToken();
    const response = await fetch(`${API_BASE}/updateRepair`, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        authorization: `Bearer ${token}`,
      },
      method: 'POST',
      body: JSON.stringify({
        ...data,
        key,
      }),
    });

    if (response.ok) {
      return Promise.resolve();
    }

    const text = await response.text();
    return Promise.reject(text);
  };

  markRepairAsComplete = id =>
    firebase
      .database()
      .ref(`/repairs/${id}/status`)
      .set(RepairStatus.COMPLETE);

  deleteRepair = id =>
    firebase
      .database()
      .ref(`/repairs/${id}`)
      .remove();

  addRepairComment = (id, commentData) =>
    firebase
      .database()
      .ref(`/repairs/${id}/comments`)
      .push(commentData);
}

const dataService = new DataService();

export default dataService;
