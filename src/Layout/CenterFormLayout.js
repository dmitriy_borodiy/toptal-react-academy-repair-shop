import React from 'react';
import PropTypes from 'prop-types';
import { Grid } from 'semantic-ui-react';

import './CenterFormLayout.css';

class CenterFormLayout extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
  };

  render() {
    return (
      <div className="CenterFormLayout">
        <Grid textAlign="center" style={{ height: '100%' }} verticalAlign="middle">
          <Grid.Column style={{ maxWidth: 450 }}>{this.props.children}</Grid.Column>
        </Grid>
      </div>
    );
  }
}

export default CenterFormLayout;
