import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Modal, Header, Button, Label, Segment, Grid, Table } from 'semantic-ui-react';

import { dataService } from '../services';
import { UsersShape } from '../shapes';
import { UserRole } from '../constants';

import EditUserFormModal from './EditUserFormModal';
import './UsersPage.css';

class UsersPage extends React.Component {
  static propTypes = {
    users: UsersShape,
    loading: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    users: [],
  };

  state = {
    editUserModalOpen: false,
    deleteUserModalOpen: false,
    editUserKey: null,
    deleteUserKey: null,
  };

  handleEditUserFormSubmit = values =>
    dataService.updateUser(this.state.editUserKey, values).then(this.closeEditUserModal);

  handleDeleteUser = () =>
    dataService.deleteUser(this.state.deleteUserKey).then(this.closeDeleteUserModal);

  closeEditUserModal = () => {
    this.setState({
      editUserModalOpen: false,
      editUserKey: null,
    });
  };

  openEditUserModal = (editUserKey = null) => {
    this.setState({
      editUserModalOpen: true,
      editUserKey,
    });
  };

  closeDeleteUserModal = () => {
    this.setState({
      deleteUserModalOpen: false,
      deleteUserKey: null,
    });
  };

  openDeleteUserModal = (deleteUserKey) => {
    this.setState({
      deleteUserModalOpen: true,
      deleteUserKey,
    });
  };

  renderEditUserModal = () => {
    const user = this.props.users.find(r => r.key === this.state.editUserKey);

    return (
      this.state.editUserModalOpen && (
        <EditUserFormModal
          users={this.props.users}
          isAddingNew={!this.state.editUserKey}
          onSubmit={this.handleEditUserFormSubmit}
          open={this.state.editUserModalOpen}
          onClose={this.closeEditUserModal}
          initialValues={{
            email: user.email,
            role: user.role,
          }}
        />
      )
    );
  };

  renderDeleteUserModal = () => (
    <Modal
      open={this.state.deleteUserModalOpen}
      onClose={this.closeDeleteUserModal}
      basic
      size="small"
    >
      <Header icon="trash outline" content="Delete this User?" />
      <Modal.Content>
        <p>This action is irreversible.</p>
      </Modal.Content>
      <Modal.Actions>
        <Button basic inverted autoFocus onClick={this.closeDeleteUserModal}>
          No
        </Button>
        <Button color="red" inverted onClick={this.handleDeleteUser}>
          <Icon name="trash outline" /> Yes, Delete
        </Button>
      </Modal.Actions>
    </Modal>
  );

  render() {
    return (
      <Segment vertical loading={this.props.loading}>
        {this.renderEditUserModal()}
        {this.renderDeleteUserModal()}

        <Grid container stackable verticalAlign="middle">
          <Grid.Row>
            <Grid.Column width={16}>
              <Table>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>Email</Table.HeaderCell>
                    <Table.HeaderCell>Role</Table.HeaderCell>
                    <Table.HeaderCell>Actions</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>

                <Table.Body>
                  {this.props.users.map(user => (
                    <Table.Row key={user.key}>
                      <Table.Cell>{user.email}</Table.Cell>
                      <Table.Cell>
                        <Label horizontal color={user.role === UserRole.MANAGER ? 'green' : 'grey'}>
                          {user.role}
                        </Label>
                      </Table.Cell>
                      <Table.Cell>
                        <Button
                          icon="pencil"
                          content="Edit"
                          size="tiny"
                          onClick={() => this.openEditUserModal(user.key)}
                        />
                        <Button
                          size="tiny"
                          negative
                          content="Delete"
                          icon="remove"
                          onClick={() => this.openDeleteUserModal(user.key)}
                        />
                      </Table.Cell>
                    </Table.Row>
                  ))}
                </Table.Body>
              </Table>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    );
  }
}

export default UsersPage;
