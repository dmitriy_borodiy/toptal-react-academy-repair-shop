import React from 'react';
import PropTypes from 'prop-types';
import { Button, Label, Form, Message, Segment, Modal } from 'semantic-ui-react';

import { UserRole } from '../constants';

const ROLES_OPTIONS = [
  {
    key: UserRole.MANAGER,
    content: <Label color="green">{UserRole.MANAGER}</Label>,
    value: UserRole.MANAGER,
  },
  {
    key: UserRole.USER,
    content: <Label color="grey">{UserRole.USER}</Label>,
    value: UserRole.USER,
  },
];

export default class EditUserFormModal extends React.Component {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    initialValues: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  };

  constructor(props) {
    super(props);

    const state = {
      submitting: false,
      hasSubmitted: false,
      values: this.props.initialValues,
      valid: true,
      error: null,
      errors: {},
    };

    const errors = this.validate(state.values);
    this.state = {
      ...state,
      errors,
      valid: Object.keys(errors).length === 0,
    };
  }

  get submitValues() {
    return {
      role: this.state.values.role || null,
      email: this.state.values.email || null,
    };
  }

  handleFieldChange = (key, value, cb) => {
    const newValues = {
      ...this.state.values,
      [key]: value,
    };

    const validationResult = this.validate(newValues);
    const valid = Object.keys(validationResult).length === 0;

    this.setState(
      {
        values: newValues,
        errors: validationResult || {},
        valid,
      },
      cb,
    );
  };

  validate = (values) => {
    const errors = {};
    if (!values.email || values.email === '') {
      errors.email = 'Is required';
    }
    if (!values.role) {
      errors.role = 'Is required';
    }

    return errors;
  };

  handleRoleChange = (event, data) => {
    this.handleFieldChange('role', data.value);
  };

  handleEmailChange = (event) => {
    this.handleFieldChange('email', event.target.value);
  };

  handleSubmit = async () => {
    if (this.state.submitting) {
      return;
    }

    if (!this.state.valid) {
      this.setState({
        error: null,
        hasSubmitted: true,
        submitting: false,
      });
      return;
    }

    this.setState({
      hasSubmitted: true,
      submitting: true,
    });

    try {
      await this.props.onSubmit(this.submitValues);
    } catch (error) {
      console.error(error);
      this.setState({
        error: error.message || 'Error while saving data',
        submitting: false,
      });
    }
  };

  render() {
    return (
      <Modal open={this.props.open} onClose={this.props.onClose}>
        <Modal.Header>Edit User</Modal.Header>
        <Modal.Content>
          <Form loading={this.state.submitting} error={!this.state.valid || !!this.state.error}>
            <Segment vertical>
              <Form.Group>
                <Form.Input
                  label="email"
                  icon="at"
                  required
                  inline
                  value={this.state.values.email}
                  onChange={this.handleEmailChange}
                  error={this.state.hasSubmitted && !!this.state.errors.email}
                />
              </Form.Group>
              {this.state.hasSubmitted &&
                this.state.errors.email && <Message error>{this.state.errors.email}</Message>}

              <Form.Group>
                <Form.Dropdown
                  label="Role"
                  required
                  error={this.state.hasSubmitted && !!this.state.errors.role}
                  inline
                  trigger={
                    <Label color={this.state.values.role === UserRole.MANAGER ? 'green' : 'grey'}>
                      {this.state.values.role}
                    </Label>
                  }
                  item
                  placeholder="Role..."
                  options={ROLES_OPTIONS}
                  onChange={this.handleRoleChange}
                  value={this.state.values.role}
                />
              </Form.Group>
              {this.state.hasSubmitted &&
                this.state.errors.role && <Message error>{this.state.errors.role}</Message>}

              {this.state.error && <Message error>{this.state.error}</Message>}
            </Segment>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button onClick={this.props.onClose}>Cancel</Button>
          <Button
            primary
            onClick={this.handleSubmit}
            disabled={this.state.submitting}
            loading={this.state.submitting}
          >
            Save changes
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}
