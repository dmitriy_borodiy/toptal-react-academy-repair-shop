import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { Visibility, Segment } from 'semantic-ui-react';

import { UserRole } from './constants';

import firebase, {
  subscribeToRefAsList,
  subscribeToRefAsObject,
  unsubscribeFromRef,
} from './firebase';

import { MenuFluid, MenuFixed } from './AppMenu';
import LoginPage from './LoginPage';
import LogoutPage from './LogoutPage';
import SignupPage from './SignupPage';
import RepairsPage from './RepairsPage';
import UsersPage from './UsersPage';

import { getFilteredRepairs } from './selectors';

import './App.css';

const ConditionalRoute = ({
  componentProps,
  redirectTo,
  redirectCondition,
  component: Component,
  ...rest
}) => (
  <Route
    {...rest}
    render={props =>
      (!redirectCondition ? (
        <Component {...props} {...componentProps} />
      ) : (
        <Redirect
          to={{
            pathname: redirectTo,
            state: { from: props.location },
          }}
        />
      ))}
  />
);

ConditionalRoute.propTypes = {
  redirectCondition: PropTypes.bool.isRequired,
  componentProps: PropTypes.object, // eslint-disable-line react/forbid-prop-types
  redirectTo: PropTypes.string.isRequired,
  component: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
  location: PropTypes.shape({
    hash: PropTypes.string.isRequired,
    pathname: PropTypes.string.isRequired,
  }).isRequired,
};

ConditionalRoute.defaultProps = {
  componentProps: {},
  location: {
    hash: '',
    pathname: '/',
  },
};

const RedirectToReparsPage = ({ location }) => (
  <Redirect
    to={{
      pathname: '/repairs',
      state: { from: location },
    }}
  />
);

RedirectToReparsPage.propTypes = {
  location: PropTypes.shape({
    hash: PropTypes.string.isRequired,
    pathname: PropTypes.string.isRequired,
  }),
};

RedirectToReparsPage.defaultProps = {
  location: {
    hash: '',
    pathname: '/',
  },
};

const initialStoreState = {
  auth: {
    user: null,
    loading: true,
  },
  users: {
    data: [],
    loading: true,
  },
  repairs: {
    data: [],
    loading: true,
    filters: null,
  },
};

export default class App extends React.Component {
  state = {
    fixedMenuVisible: false,
    store: initialStoreState,
  };

  componentDidMount() {
    firebase.auth().onAuthStateChanged((authInfo) => {
      if (!authInfo) {
        this.handleAuthStateChanged(null);
        return;
      }
      subscribeToRefAsObject(firebase.database().ref(`/users/${authInfo.uid}`), (user) => {
        this.handleAuthStateChanged(user);
      });
    });
  }

  get userRole() {
    if (!this.state.store.auth.user) {
      return null;
    }

    return this.state.store.auth.user.role;
  }

  handleAuthStateChanged = async (user) => {
    if (user) {
      const token = await firebase.auth().currentUser.getIdToken();
      const repairsRef = firebase.database().ref('repairs/');

      subscribeToRefAsList(firebase.database().ref('users/'), this.handleUsersUpdated);

      if (user.role === UserRole.MANAGER) {
        subscribeToRefAsList(repairsRef, this.handleRepairsUpdated);
      } else {
        subscribeToRefAsList(
          repairsRef.orderByChild('userKey').equalTo(user.key),
          this.handleRepairsUpdated,
        );
      }

      this.setState(state => ({
        ...state,
        store: {
          ...state.store,
          auth: {
            token,
            user,
            loading: false,
          },
        },
      }));
    } else {
      if (this.state.store.auth.user) {
        unsubscribeFromRef(firebase.database().ref(`/users/${this.state.store.auth.user.key}`));
        unsubscribeFromRef(firebase.database().ref('repairs'));
        if (this.state.store.auth.user.role === UserRole.MANAGER) {
          unsubscribeFromRef(firebase.database().ref('users'));
        }
      }

      this.setState(state => ({
        ...state,
        store: {
          ...initialStoreState,
          auth: {
            user: null,
            loading: false,
          },
        },
      }));
    }
  };

  handleUsersUpdated = (data) => {
    this.setState(state => ({
      ...state,
      store: {
        ...state.store,
        users: {
          data,
          loading: false,
        },
      },
    }));
  };

  handleRepairsUpdated = (data) => {
    this.setState(state => ({
      ...state,
      store: {
        ...state.store,
        repairs: {
          ...state.store.repairs,
          data: this.transformRepairsData(data),
          loading: false,
        },
      },
    }));
  };

  transformRepairsData = data =>
    data
      .map((repair) => {
        const commentsTransformed = Object.keys(repair.comments || {})
          .map(key => ({
            ...repair.comments[key],
            key,
          }))
          .sort((a, b) => b.date - a.date);
        return {
          ...repair,
          comments: commentsTransformed,
        };
      })
      .sort((a, b) => b.time - a.time);

  showFixedMenu = () => {
    this.setState({ fixedMenuVisible: true });
  };

  hideFixedMenu = () => {
    this.setState({ fixedMenuVisible: false });
  };

  isAuthenticated = () => !!this.state.store.auth.user;
  notAuthenticated = () => !this.state.store.auth.user;
  isManager = () =>
    !!this.state.store.auth.user && this.state.store.auth.user.role === UserRole.MANAGER;

  handleRepairFiltersChange = (filters) => {
    this.setState(state => ({
      ...state,
      store: {
        ...state.store,
        repairs: {
          ...state.store.repairs,
          filters,
        },
      },
    }));
  };

  render() {
    return (
      <Router>
        <div>
          {this.state.fixedMenuVisible ? (
            <MenuFixed
              isAuthenticated={!!this.state.store.auth.user}
              loading={this.state.store.auth.loading}
              userRole={this.userRole}
            />
          ) : null}

          <Visibility
            onBottomPassed={this.showFixedMenu}
            onBottomVisible={this.hideFixedMenu}
            once={false}
          >
            <MenuFluid
              isAuthenticated={!!this.state.store.auth.user}
              loading={this.state.store.auth.loading}
              userRole={this.userRole}
            />
          </Visibility>

          <Segment vertical loading={this.state.store.auth.loading} className="App-main-segment">
            {!this.state.store.auth.loading && (
              <Switch>
                <Route exact path="/" component={RedirectToReparsPage} />
                <ConditionalRoute
                  path="/login"
                  component={LoginPage}
                  redirectTo="/repairs"
                  redirectCondition={this.isAuthenticated()}
                />
                <ConditionalRoute
                  path="/signup"
                  component={SignupPage}
                  redirectTo="/repairs"
                  redirectCondition={this.isAuthenticated()}
                />
                <Route path="/logout" component={LogoutPage} />
                <ConditionalRoute
                  path="/repairs"
                  component={RepairsPage}
                  redirectTo="/login"
                  redirectCondition={this.notAuthenticated()}
                  componentProps={{
                    user: this.state.store.auth.user,
                    repairs: getFilteredRepairs(
                      this.state.store.repairs.data,
                      this.state.store.repairs.filters,
                    ),
                    filters: this.state.store.repairs.filters,
                    users: this.state.store.users.data,
                    loading: this.state.store.repairs.loading || this.state.store.users.loading,
                    onFiltersChange: this.handleRepairFiltersChange,
                  }}
                />
                <ConditionalRoute
                  path="/users"
                  component={UsersPage}
                  redirectTo="/repairs"
                  redirectCondition={this.notAuthenticated() || !this.isManager()}
                  componentProps={{
                    users: this.state.store.users.data,
                    loading: this.state.store.users.loading,
                  }}
                />
              </Switch>
            )}
          </Segment>
        </div>
      </Router>
    );
  }
}
