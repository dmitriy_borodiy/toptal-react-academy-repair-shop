import React from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import { Menu } from 'semantic-ui-react';

class MenuLink extends React.Component {
  static propTypes = {
    to: PropTypes.string.isRequired,
    children: PropTypes.node.isRequired,
    location: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
    visible: PropTypes.bool,
    match: PropTypes.any, // eslint-disable-line react/forbid-prop-types
    history: PropTypes.any, // eslint-disable-line react/forbid-prop-types
    staticContext: PropTypes.any, // eslint-disable-line react/forbid-prop-types
  };

  static defaultProps = {
    visible: true,
    match: null,
    history: null,
    staticContext: null,
  };

  render() {
    const {
      match,
      history,
      staticContext,
      visible,
      to,
      location,
      children,
      ...otherProps
    } = this.props;

    if (!visible) {
      return null;
    }

    return (
      <Menu.Item as={Link} to={to} active={location.pathname === to} {...otherProps}>
        {children}
      </Menu.Item>
    );
  }
}

export default withRouter(MenuLink);
