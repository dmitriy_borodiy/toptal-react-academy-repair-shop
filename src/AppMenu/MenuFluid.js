import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Menu, Segment, Container, Button } from 'semantic-ui-react';

import { UserRoleShape } from '../shapes';
import { UserRole } from '../constants';

import MenuLink from './MenuLink';

class MenuFixed extends React.Component {
  static propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    loading: PropTypes.bool.isRequired,
    userRole: UserRoleShape,
  };

  static defaultProps = {
    userRole: null,
  };

  render() {
    const { isAuthenticated, loading, userRole } = this.props;

    return (
      <Segment inverted textAlign="center" vertical>
        <Container>
          <Menu inverted pointing secondary size="large">
            <Menu.Item as={MenuLink} to="/repairs" visible={!loading && isAuthenticated}>
              {userRole === UserRole.MANAGER ? 'Repairs' : 'My Repairs'}
            </Menu.Item>
            <Menu.Item
              as={MenuLink}
              to="/users"
              visible={!loading && isAuthenticated && userRole === UserRole.MANAGER}
            >
              Users
            </Menu.Item>
            <Menu.Item position="right">
              {!loading &&
                !isAuthenticated && (
                  <Button as={Link} inverted to="/login">
                    Log In
                  </Button>
                )}
              {!loading &&
                !isAuthenticated && (
                  <Button as={Link} inverted to="/signup" style={{ marginLeft: '0.5em' }}>
                    Sign Up
                  </Button>
                )}
              {!loading &&
                isAuthenticated && (
                  <Button as={Link} inverted to="/logout">
                    Log Out
                  </Button>
                )}
            </Menu.Item>
          </Menu>
        </Container>
      </Segment>
    );
  }
}

export default MenuFixed;
