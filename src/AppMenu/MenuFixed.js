import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Menu, Button, Container } from 'semantic-ui-react';

import { UserRoleShape } from '../shapes';
import { UserRole } from '../constants';

import MenuLink from './MenuLink';

class MenuFixed extends React.Component {
  static propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    loading: PropTypes.bool.isRequired,
    userRole: UserRoleShape,
  };

  static defaultProps = {
    userRole: null,
  };

  render() {
    const { isAuthenticated, loading, userRole } = this.props;

    return (
      <Menu fixed="top" size="large">
        <Container>
          <Menu.Item as={MenuLink} to="/repairs" visible={!loading && isAuthenticated}>
            {userRole === UserRole.MANAGER ? 'Repairs' : 'My Repairs'}
          </Menu.Item>
          <Menu.Item
            as={MenuLink}
            to="/users"
            visible={!loading && isAuthenticated && userRole === UserRole.MANAGER}
          >
            Users
          </Menu.Item>
          <Menu.Menu position="right">
            {!loading &&
              !isAuthenticated && (
                <Menu.Item className="item">
                  <Button as={Link} to="/login">
                    Log in
                  </Button>
                </Menu.Item>
              )}
            {!loading &&
              !isAuthenticated && (
                <Menu.Item>
                  <Button as={Link} to="/signup" primary>
                    Sign Up
                  </Button>
                </Menu.Item>
              )}
            {!loading &&
              isAuthenticated && (
                <Menu.Item>
                  <Button as={Link} to="/logout" primary>
                    Log Out
                  </Button>
                </Menu.Item>
              )}
          </Menu.Menu>
        </Container>
      </Menu>
    );
  }
}

export default MenuFixed;
