import moment from 'moment';

function getTime({ date, hours, minutes, amPm }) {
  if (!date) {
    return null;
  }

  const timeStr = `${moment(date).format('MM/DD/YYYY')} ${hours}:${minutes} ${amPm}`;

  const momentStart = moment(timeStr, ['MM/DD/YYYY h:m A']);
  return momentStart.toDate().getTime();
}

export const getFilteredRepairs = (repairs, filters) => {
  if (!filters) {
    return repairs;
  }

  const {
    dateStart,
    dateEnd,

    hoursStart,
    minutesStart,
    amPmStart,

    hoursEnd,
    minutesEnd,
    amPmEnd,

    userKey,
    status,
  } = filters;

  const startTime = getTime({
    date: dateStart,
    hours: hoursStart || 0,
    minutes: minutesStart || 0,
    amPm: amPmStart || 'AM',
  });

  const endTime = getTime({
    date: dateEnd,
    hours: hoursEnd || 11,
    minutes: minutesEnd || 55,
    amPm: amPmEnd || 'PM',
  });

  return repairs.filter((repair) => {
    if (startTime != null && repair.time < startTime) {
      return false;
    }
    if (endTime != null && repair.time > endTime) {
      return false;
    }
    if (userKey != null && repair.userKey !== userKey) {
      return false;
    }
    if (status != null && repair.status !== status) {
      return false;
    }

    return true;
  });
};
