export const RepairStatus = {
  COMPLETE: 'complete',
  INCOMPLETE: 'incomplete',
};

export const UserRole = {
  USER: 'user',
  MANAGER: 'manager',
};

export const RepairFiltersNoUserAssigned = '__filter:userKey:no-one-assigned__';

export const RepairFiltersEmptyValues = {
  status: null,
  userKey: null,

  dateStart: null,
  hoursStart: null,
  minutesStart: null,
  amPmStart: 'AM',

  dateEnd: null,
  hoursEnd: null,
  minutesEnd: null,
  amPmEnd: 'AM',
};
