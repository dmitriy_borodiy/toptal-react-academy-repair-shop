import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  Message,
  Popup,
  Header,
  Label,
  Icon,
  Button,
  Segment,
  Grid,
  Table,
  Modal,
} from 'semantic-ui-react';

import { RepairsShape, UsersShape, UserShape, FiltersShape } from '../shapes';
import { RepairStatus, UserRole } from '../constants';
import { dataService } from '../services';

import Comment from './Comment';
import EditRepairFormModal from './EditRepairFormModal';
import FiltersModal from './FiltersModal';
import NewCommentForm from './NewCommentForm';
import './RepairsPage.css';

class RepairsPage extends React.Component {
  static propTypes = {
    user: UserShape.isRequired,
    repairs: RepairsShape,
    users: UsersShape,
    loading: PropTypes.bool.isRequired,
    filters: FiltersShape,
    onFiltersChange: PropTypes.func.isRequired,
  };

  static defaultProps = {
    repairs: [],
    users: [],
    filters: null,
  };

  state = {
    filtersModalOpen: false,
    editRepairModalOpen: false,
    deleteRepairModalOpen: false,
    commentsModalOpen: false,
    editRepairKey: null,
    deleteRepairKey: null,
    commentsModalRepairKey: null,
  };

  get areFiltersEmpty() {
    if (!this.props.filters) {
      return true;
    }
    const nonEmptyFilterKeys = Object.keys(this.props.filters)
      .map(k => this.props.filters[k])
      .filter(v => v !== null);
    return nonEmptyFilterKeys.length === 0;
  }

  closeEditRepairModal = () => {
    this.setState({
      editRepairModalOpen: false,
      editRepairKey: null,
    });
  };

  openNewRepairModal = () => {
    this.openEditRepairModal();
  };

  openEditRepairModal = (editRepairKey = null) => {
    this.setState({
      editRepairModalOpen: true,
      editRepairKey,
    });
  };

  closeFiltersModal = () => {
    this.setState({
      filtersModalOpen: false,
    });
  };

  openFiltersModal = () => {
    this.setState({
      filtersModalOpen: true,
    });
  };

  closeDeleteRepairModal = () => {
    this.setState({
      deleteRepairModalOpen: false,
      deleteRepairKey: null,
    });
  };

  openDeleteRepairModal = (deleteRepairKey) => {
    this.setState({
      deleteRepairModalOpen: true,
      deleteRepairKey,
    });
  };

  closeCommentsModal = () => {
    this.setState({
      commentsModalOpen: false,
      commentsModalRepairKey: null,
    });
  };

  openCommentsModal = (commentsModalRepairKey) => {
    this.setState({
      commentsModalOpen: true,
      commentsModalRepairKey,
    });
  };

  markRepairAsComplete = repairKey => dataService.markRepairAsComplete(repairKey);

  handleClearFiltersClick = () => this.handleFiltersModalSubmit(null);

  handleFiltersModalSubmit = values =>
    Promise.resolve()
      .then(() => {
        if (this.props.onFiltersChange) {
          this.props.onFiltersChange(values);
        }
      })
      .then(this.closeFiltersModal);

  handleCreateRepairFormSubmit = (values) => {
    let promise;
    if (this.state.editRepairKey) {
      promise = dataService.updateRepair(this.state.editRepairKey, values);
    } else {
      promise = dataService.createRepair(values);
    }
    return promise.then(this.closeEditRepairModal);
  };

  handleDeleteRepair = () =>
    dataService.deleteRepair(this.state.deleteRepairKey).then(this.closeDeleteRepairModal);

  handleSubmitCommentForm = data =>
    dataService.addRepairComment(this.state.commentsModalRepairKey, {
      text: data.text,
      userKey: this.props.user.key,
      date: new Date().getTime(),
    });

  renderCreateRepairModal = () => {
    let initialValues;
    if (this.state.editRepairKey) {
      const repair = this.props.repairs.find(r => r.key === this.state.editRepairKey);
      initialValues = {
        date: repair.time ? moment(repair.time) : null,
        userKey: repair.userKey,
        status: repair.status,
        approved: repair.approved || false,
        hours: repair.time ? parseInt(moment(repair.time).format('hh'), 10) : null,
        minutes: repair.time ? moment(repair.time).minutes() : null,
        amPm: repair.time ? moment(repair.time).format('A') : null,
      };
    }
    return (
      this.state.editRepairModalOpen && (
        <EditRepairFormModal
          users={this.props.users}
          isAddingNew={!this.state.editRepairKey}
          onSubmit={this.handleCreateRepairFormSubmit}
          open={this.state.editRepairModalOpen}
          onClose={this.closeEditRepairModal}
          initialValues={initialValues}
        />
      )
    );
  };

  renderFiltersModal = () =>
    this.state.filtersModalOpen && (
      <FiltersModal
        hideUserFilter={this.props.user.role !== UserRole.MANAGER}
        users={this.props.users}
        onSubmit={this.handleFiltersModalSubmit}
        open={this.state.filtersModalOpen}
        onClose={this.closeFiltersModal}
        filters={this.props.filters}
      />
    );

  renderDeleteRepairModal = () => (
    <Modal
      open={this.state.deleteRepairModalOpen}
      onClose={this.closeDeleteRepairModal}
      basic
      size="small"
    >
      <Header icon="trash outline" content="Delete this Repair?" />
      <Modal.Content>
        <p>This action is irreversible.</p>
      </Modal.Content>
      <Modal.Actions>
        <Button basic inverted autoFocus onClick={this.closeDeleteRepairModal}>
          No
        </Button>
        <Button color="red" inverted onClick={this.handleDeleteRepair}>
          <Icon name="trash outline" /> Yes, Delete
        </Button>
      </Modal.Actions>
    </Modal>
  );

  renderCommentsModal = () => {
    const repair = this.props.repairs.find(r => r.key === this.state.commentsModalRepairKey);
    if (!repair) {
      return null;
    }
    const comments = repair.comments || [];
    return (
      <Modal
        open={this.state.commentsModalOpen}
        size="small"
        onClose={this.closeCommentsModal}
        closeOnDimmerClick
        closeIcon="close"
      >
        <Header icon="pencil" content="Comments" />
        <Modal.Content>
          <h3>New Comment</h3>
          <NewCommentForm onSubmit={this.handleSubmitCommentForm} />
          <h3>Comments:</h3>
          {comments.length > 0 ? (
            comments.map((comment) => {
              const user = this.props.users.find(u => u.key === comment.userKey);
              return (
                <Comment key={comment.key} user={user} text={comment.text} date={comment.date} />
              );
            })
          ) : (
            <p>There are no comments yet.</p>
          )}
        </Modal.Content>
      </Modal>
    );
  };

  render() {
    const { user, users, loading } = this.props;
    return (
      <Segment vertical loading={loading}>
        {this.renderCreateRepairModal()}
        {this.renderDeleteRepairModal()}
        {this.renderCommentsModal()}
        {this.renderFiltersModal()}

        <Grid container stackable verticalAlign="middle">
          <Grid.Row>
            <Grid.Column width={16}>
              {user.role === UserRole.MANAGER && (
                <Button
                  icon="plus"
                  content="Create Repair"
                  positive
                  onClick={this.openNewRepairModal}
                />
              )}

              <Button
                icon="filter"
                color={this.areFiltersEmpty ? 'grey' : 'teal'}
                content="Filters"
                onClick={this.openFiltersModal}
              />
              {!this.areFiltersEmpty && (
                <Button content="Clear filters" onClick={this.handleClearFiltersClick} />
              )}

              {this.props.repairs.length > 0 ? (
                <Table>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell>Time</Table.HeaderCell>
                      {user.role === UserRole.MANAGER && (
                        <Table.HeaderCell>User Assigned</Table.HeaderCell>
                      )}
                      <Table.HeaderCell>Status</Table.HeaderCell>
                      <Table.HeaderCell>Actions</Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>

                  <Table.Body>
                    {this.props.repairs.map((repair) => {
                      const assignedUser = users.find(u => u.key === repair.userKey);
                      return (
                        <Table.Row key={repair.key}>
                          <Table.Cell>
                            {repair.time ? moment(repair.time).format('MM/DD/YYYY hh:mm A') : '-'}
                          </Table.Cell>
                          {user.role === UserRole.MANAGER && (
                            <Table.Cell>{assignedUser ? assignedUser.email : '-'}</Table.Cell>
                          )}
                          <Table.Cell>
                            <Label
                              horizontal
                              color={repair.status === RepairStatus.COMPLETE ? 'green' : 'orange'}
                            >
                              {repair.status}
                            </Label>
                            {repair.approved && (
                              <Popup
                                trigger={<Icon color="green" name="check" />}
                                content="Approved by managers"
                              />
                            )}
                          </Table.Cell>
                          <Table.Cell>
                            {user.role === UserRole.MANAGER && (
                              <Button
                                size="tiny"
                                icon="pencil"
                                content="Edit"
                                onClick={() => this.openEditRepairModal(repair.key)}
                              />
                            )}
                            <Button size="tiny" onClick={() => this.openCommentsModal(repair.key)}>
                              Comments ({repair.comments ? repair.comments.length : 0})
                            </Button>
                            {user.role === UserRole.MANAGER && (
                              <Button
                                size="tiny"
                                negative
                                icon="remove"
                                content="Delete"
                                onClick={() => this.openDeleteRepairModal(repair.key)}
                              />
                            )}
                            {user.role === UserRole.USER &&
                              repair.status === RepairStatus.INCOMPLETE && (
                                <Button
                                  size="tiny"
                                  icon="checkmark"
                                  content="Mark Complete"
                                  primary
                                  onClick={() => this.markRepairAsComplete(repair.key)}
                                />
                              )}
                          </Table.Cell>
                        </Table.Row>
                      );
                    })}
                  </Table.Body>
                </Table>
              ) : (
                <Message info>
                  {this.areFiltersEmpty &&
                    (user.role === UserRole.USER
                      ? "You don't have any repairs assigned to you."
                      : 'There are no repairs created')}
                  {!this.areFiltersEmpty && 'No repairs matching the filters'}
                </Message>
              )}
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    );
  }
}

export default RepairsPage;
