import React from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Message } from 'semantic-ui-react';

const initialValues = {
  text: '',
};

export default class NewCommentForm extends React.Component {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    const state = {
      submitting: false,
      hasSubmitted: false,
      values: initialValues,
      valid: true,
      error: null,
      errors: {},
    };

    const errors = this.validate(state.values);
    this.state = {
      ...state,
      errors,
      valid: Object.keys(errors).length === 0,
    };
  }

  handleFieldChange = (key, value) => {
    const newValues = {
      ...this.state.values,
      [key]: value,
    };

    const validationResult = this.validate(newValues);
    const valid = Object.keys(validationResult).length === 0;

    this.setState({
      values: newValues,
      errors: validationResult || {},
      valid,
      error: null,
    });
  };

  validate = (values) => {
    const errors = {};
    if (!values.text || values.text.length === '') {
      errors.text = 'Is required';
    }
    return errors;
  };

  handleTextChange = event => this.handleFieldChange('text', event.target.value);

  handleSubmit = async () => {
    if (!this.state.valid) {
      this.setState({
        error: null,
        hasSubmitted: true,
      });
      return;
    }

    this.setState({
      hasSubmitted: true,
      submitting: true,
    });

    try {
      const { text } = this.state.values;
      await this.props.onSubmit({ text });
      this.setState({
        submitting: false,
        values: initialValues,
      });
    } catch (error) {
      console.error(error);
      this.setState({
        error: error.message || 'Error while adding comment',
        submitting: false,
      });
    }
  };

  render() {
    return (
      <Form
        size="large"
        loading={this.state.submitting}
        error={!this.state.valid || !!this.state.error}
      >
        <Form.TextArea
          autoFocus
          error={this.state.hasSubmitted && this.state.errors.text}
          required
          placeholder="Enter your comment..."
          onChange={this.handleTextChange}
          value={this.state.values.text}
        />

        {this.state.error && <Message error>{this.state.error}</Message>}

        <Button
          color="teal"
          fluid
          size="large"
          onClick={this.handleSubmit}
          loading={this.state.submitting}
        >
          Add Comment
        </Button>
      </Form>
    );
  }
}
