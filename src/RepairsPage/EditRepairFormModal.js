import React from 'react';
import PropTypes from 'prop-types';
import { SingleDatePicker } from 'react-dates';
import { Button, Label, Form, Message, Segment, Modal } from 'semantic-ui-react';
import moment from 'moment';

import { UsersShape } from '../shapes';
import { RepairStatus } from '../constants';

function alwaysFalse() {
  return false;
}

const AM_PM_OPTIONS = [
  {
    key: 'AM',
    text: 'AM',
    value: 'AM',
  },
  {
    key: 'PM',
    text: 'PM',
    value: 'PM',
  },
];

const HOURS_OPTIONS = [];
for (let hour = 1; hour <= 12; hour += 1) {
  HOURS_OPTIONS.push({
    key: hour,
    value: hour,
    text: hour.toString().padStart(2, '0'),
  });
}

const MINUTES_OPTIONS = [];
for (let minute = 0; minute < 60; minute += 5) {
  MINUTES_OPTIONS.push({
    key: minute,
    value: minute,
    text: minute.toString().padStart(2, '0'),
  });
}

export default class EditRepairFormModal extends React.Component {
  static propTypes = {
    isAddingNew: PropTypes.bool.isRequired,
    onSubmit: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    initialValues: PropTypes.object, // eslint-disable-line react/forbid-prop-types
    users: UsersShape.isRequired,
  };

  static defaultProps = {
    initialValues: {
      date: moment(),
      status: RepairStatus.INCOMPLETE,
      hours: 12,
      minutes: 0,
      amPm: 'PM',
    },
  };

  constructor(props) {
    super(props);

    const state = {
      submitting: false,
      hasSubmitted: false,
      datePickerFocused: false,
      values: this.props.initialValues,
      valid: true,
      error: null,
      errors: {},
    };

    const errors = this.validate(state.values);
    this.state = {
      ...state,
      errors,
      valid: Object.keys(errors).length === 0,
    };
  }

  get submitValues() {
    const hours = this.state.values.hours || 12;
    const minutes = this.state.values.minutes || 0;
    const amPm = this.state.values.amPm || 'PM';
    const date = moment(
      this.state.values.date ? this.state.values.date.toDate().getTime() : new Date().getTime(),
    );
    const timeStr = `${date.format('MM/DD/YYYY')} ${hours}:${minutes} ${amPm}`;
    const time = moment(timeStr, ['MM/DD/YYYY h:m A']);
    return {
      status: this.state.values.status || null,
      userKey: this.state.values.userKey || null,
      approved:
        this.state.values.status === RepairStatus.COMPLETE
          ? this.state.values.approved || false
          : false,
      time: time.toDate().getTime(),
    };
  }

  get userOptions() {
    return [
      {
        key: '__empty__',
        text: 'No one assigned',
        value: null,
      },
    ].concat(
      this.props.users.map(user => ({
        key: user.key,
        text: user.email,
        value: user.key,
      })),
    );
  }

  handleFieldChange = (key, value, cb) => {
    const newValues = {
      ...this.state.values,
      [key]: value,
    };

    const validationResult = this.validate(newValues);
    const valid = Object.keys(validationResult).length === 0;

    this.setState(
      {
        values: newValues,
        errors: validationResult || {},
        valid,
      },
      cb,
    );
  };

  validate = (values) => {
    const errors = {};
    if (!values.status || values.status === '') {
      errors.status = 'Is required';
    }
    if (!values.date) {
      errors.date = 'Is required';
    } else if (!values.date.isValid()) {
      errors.date = 'Date is invalid';
    }

    return errors;
  };

  handleStatusChange = (event, data) => {
    this.handleFieldChange('status', data.value, () => {
      if (data.value === RepairStatus.INCOMPLETE) {
        this.handleFieldChange('approved', false);
      }
    });
  };

  handleUserAssignedChange = (event, data) => {
    this.handleFieldChange('userKey', data.value);
  };

  handleDateChange = (data) => {
    this.handleFieldChange('date', data);
  };

  handleApprovedChange = (event, data) => {
    this.handleFieldChange('approved', data.checked);
  };

  handleAmPmChange = (event, data) => {
    this.handleFieldChange('amPm', data.value);
  };

  handleHoursChange = (event, data) => {
    this.handleFieldChange('hours', data.value);
  };

  handleMinutesChange = (event, data) => {
    this.handleFieldChange('minutes', data.value);
  };

  handleSubmit = async () => {
    if (this.state.submitting) {
      return;
    }

    if (!this.state.valid) {
      this.setState({
        error: null,
        hasSubmitted: true,
        submitting: false,
      });
      return;
    }

    this.setState({
      hasSubmitted: true,
      submitting: true,
    });

    try {
      await this.props.onSubmit(this.submitValues);
    } catch (error) {
      this.setState({
        error: error || 'Error while saving data',
        submitting: false,
      });
    }
  };

  handleDatePickerFocusChange = ({ focused }) => {
    this.setState({
      datePickerFocused: focused,
    });
  };

  render() {
    return (
      <Modal open={this.props.open} onClose={this.props.onClose}>
        <Modal.Header>{this.props.isAddingNew ? 'Add a New Repair' : 'Edit Repair'}</Modal.Header>
        <Modal.Content>
          <Form loading={this.state.submitting} error={!this.state.valid || !!this.state.error}>
            <Segment vertical>
              <Form.Group>
                <Form.Field
                  required
                  inline
                  error={this.state.hasSubmitted && !!this.state.errors.date}
                >
                  <label htmlFor="date">Date</label>
                  <SingleDatePicker
                    id="date"
                    isOutsideRange={alwaysFalse}
                    placeholder="MM/DD/YYYY"
                    date={this.state.values.date}
                    onDateChange={this.handleDateChange}
                    focused={this.state.datePickerFocused}
                    onFocusChange={this.handleDatePickerFocusChange}
                    numberOfMonths={1}
                    showClearDate
                    keepOpenOnDateSelect
                  />
                </Form.Field>

                <Form.Dropdown
                  error={this.state.hasSubmitted && !!this.state.errors.hours}
                  required
                  scrolling
                  label="Hours"
                  item
                  placeholder="-"
                  options={HOURS_OPTIONS}
                  onChange={this.handleHoursChange}
                  value={this.state.values.hours}
                />

                <Form.Dropdown
                  error={this.state.hasSubmitted && !!this.state.errors.minutes}
                  required
                  scrolling
                  label="Minutes"
                  item
                  placeholder="-"
                  options={MINUTES_OPTIONS}
                  onChange={this.handleMinutesChange}
                  value={this.state.values.minutes}
                />

                <Form.Dropdown
                  error={this.state.hasSubmitted && !!this.state.errors.amPm}
                  required
                  label="AM/PM"
                  item
                  placeholder="-"
                  options={AM_PM_OPTIONS}
                  onChange={this.handleAmPmChange}
                  value={this.state.values.amPm}
                />
              </Form.Group>
              {this.state.hasSubmitted &&
                this.state.errors.date && <Message error>{this.state.errors.date}</Message>}

              <Form.Dropdown
                error={this.state.hasSubmitted && !!this.state.errors.userKey}
                label="User Assigned"
                selection
                search
                placeholder="No one assigned"
                options={this.userOptions}
                onChange={this.handleUserAssignedChange}
                value={this.state.values.userKey}
              />
              {this.state.hasSubmitted &&
                this.state.errors.status && <Message error>{this.state.errors.status}</Message>}

              <Form.Dropdown
                label="Status"
                required
                error={this.state.hasSubmitted && !!this.state.errors.status}
                inline
                trigger={
                  <Label
                    color={this.state.values.status === RepairStatus.COMPLETE ? 'green' : 'orange'}
                  >
                    {this.state.values.status}
                  </Label>
                }
                item
                placeholder="Status..."
                options={[
                  {
                    key: RepairStatus.INCOMPLETE,
                    value: RepairStatus.INCOMPLETE,
                    content: <Label color="orange">incomplete</Label>,
                  },
                  {
                    key: RepairStatus.COMPLETE,
                    value: RepairStatus.COMPLETE,
                    content: <Label color="green">complete</Label>,
                  },
                ]}
                onChange={this.handleStatusChange}
                value={this.state.values.status}
              />
              {this.state.hasSubmitted &&
                this.state.errors.status && <Message error>{this.state.errors.status}</Message>}

              <Form.Checkbox
                inline
                label="Approved by managers?"
                disabled={this.state.values.status !== RepairStatus.COMPLETE}
                onChange={this.handleApprovedChange}
                checked={this.state.values.approved}
              />

              {this.state.error && <Message error>{this.state.error}</Message>}
            </Segment>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button onClick={this.props.onClose}>Cancel</Button>
          <Button
            primary
            onClick={this.handleSubmit}
            disabled={this.state.submitting}
            loading={this.state.submitting}
          >
            {this.props.isAddingNew ? 'Save Repair' : 'Save Changes'}
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}
