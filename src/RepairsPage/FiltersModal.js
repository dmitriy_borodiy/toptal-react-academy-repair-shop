import React from 'react';
import PropTypes from 'prop-types';
import { SingleDatePicker } from 'react-dates';
import { Button, Label, Form, Message, Segment, Modal } from 'semantic-ui-react';
import moment from 'moment';

import { UsersShape, FiltersShape } from '../shapes';
import { RepairStatus, RepairFiltersEmptyValues, RepairFiltersNoUserAssigned } from '../constants';

function alwaysFalse() {
  return false;
}

const AM_PM_OPTIONS = [
  {
    key: 'AM',
    text: 'AM',
    value: 'AM',
  },
  {
    key: 'PM',
    text: 'PM',
    value: 'PM',
  },
];

const HOURS_OPTIONS = [
  {
    key: 'no-filter',
    value: null,
    text: 'Any',
  },
];
for (let hour = 1; hour <= 12; hour += 1) {
  HOURS_OPTIONS.push({
    key: hour,
    value: hour,
    text: hour.toString().padStart(2, '0'),
  });
}

const MINUTES_OPTIONS = [
  {
    key: 'no-filter',
    value: null,
    text: 'Any',
  },
];
for (let minute = 0; minute < 60; minute += 5) {
  MINUTES_OPTIONS.push({
    key: minute,
    value: minute,
    text: minute.toString().padStart(2, '0'),
  });
}

const STATUS_OPTIONS = [
  {
    key: 'no-filter',
    value: null,
    content: 'No filter',
  },
  {
    key: RepairStatus.INCOMPLETE,
    value: RepairStatus.INCOMPLETE,
    content: <Label color="orange">incomplete</Label>,
  },
  {
    key: RepairStatus.COMPLETE,
    value: RepairStatus.COMPLETE,
    content: <Label color="green">complete</Label>,
  },
];

export default class FiltersModal extends React.Component {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    hideUserFilter: PropTypes.bool,
    onClose: PropTypes.func.isRequired,
    filters: FiltersShape,
    users: UsersShape.isRequired,
  };

  static defaultProps = {
    filters: {},
    hideUserFilter: false,
  };

  constructor(props) {
    super(props);

    const filters = this.props.filters;
    const initialValues = {
      ...filters,
      dateStart: filters && filters.dateStart ? moment(filters.dateStart) : null,
      dateEnd: filters && filters.dateEnd ? moment(filters.dateEnd) : null,
    };

    const state = {
      submitting: false,
      hasSubmitted: false,
      datePickerStartFocused: false,
      datePickerEndFocused: false,
      values: {
        ...RepairFiltersEmptyValues,
        ...initialValues,
      },
      valid: true,
      error: null,
      errors: {},
    };

    const errors = this.validate(state.values);
    this.state = {
      ...state,
      errors,
      valid: Object.keys(errors).length === 0,
    };
  }

  get submitValues() {
    const {
      status,
      userKey,
      hoursStart,
      minutesStart,
      amPmStart,
      hoursEnd,
      minutesEnd,
      amPmEnd,
      dateStart,
      dateEnd,
    } = this.state.values;
    return {
      status,
      userKey,

      dateStart: dateStart ? dateStart.toDate().getTime() : null,
      dateEnd: dateEnd ? dateEnd.toDate().getTime() : null,

      hoursStart: !dateStart || hoursStart == null ? null : parseInt(hoursStart, 10),
      minutesStart: !dateStart || minutesStart == null ? null : parseInt(minutesStart, 10),
      amPmStart: dateStart ? amPmStart : null,

      hoursEnd: !dateEnd || hoursEnd == null ? null : parseInt(hoursEnd, 10),
      minutesEnd: !dateEnd || minutesEnd == null ? null : parseInt(minutesEnd, 10),
      amPmEnd: dateEnd ? amPmEnd : null,
    };
  }

  get userOptions() {
    return [
      {
        key: 'no-filter',
        text: 'No filter',
        value: null,
      },
      {
        key: RepairFiltersNoUserAssigned,
        text: 'No one assigned',
        value: RepairFiltersNoUserAssigned,
      },
    ].concat(
      this.props.users.map(user => ({
        key: user.key,
        text: user.email,
        value: user.key,
      })),
    );
  }

  handleClearFiltersClick = () => {
    this.setState({
      values: RepairFiltersEmptyValues,
      errors: {},
      valid: true,
    });
  };

  handleFieldChange = (key, value, cb) => {
    const newValues = {
      ...this.state.values,
      [key]: value,
    };

    const validationResult = this.validate(newValues);
    const valid = Object.keys(validationResult).length === 0;

    this.setState(
      {
        values: newValues,
        errors: validationResult || {},
        valid,
      },
      cb,
    );
  };

  validate = (values) => {
    const errors = {};
    if (values.dateStart && !values.dateStart.isValid()) {
      errors.dateStart = 'Date is invalid';
    }

    if (values.dateEnd && !values.dateEnd.isValid()) {
      errors.dateEnd = 'Date is invalid';
    }

    return errors;
  };

  handleStatusChange = (event, data) => {
    this.handleFieldChange('status', data.value);
  };

  handleUserAssignedChange = (event, data) => {
    this.handleFieldChange('userKey', data.value);
  };

  handleDateStartChange = (data) => {
    this.handleFieldChange('dateStart', data);
  };

  handleAmPmStartChange = (event, data) => {
    this.handleFieldChange('amPmStart', data.value);
  };

  handleHoursStartChange = (event, data) => {
    this.handleFieldChange('hoursStart', data.value);
  };

  handleMinutesStartChange = (event, data) => {
    this.handleFieldChange('minutesStart', data.value);
  };

  handleDateEndChange = (data) => {
    this.handleFieldChange('dateEnd', data);
  };

  handleAmPmEndChange = (event, data) => {
    this.handleFieldChange('amPmEnd', data.value);
  };

  handleHoursEndChange = (event, data) => {
    this.handleFieldChange('hoursEnd', data.value);
  };

  handleMinutesEndChange = (event, data) => {
    this.handleFieldChange('minutesEnd', data.value);
  };

  handleSubmit = async () => {
    if (this.state.submitting) {
      return;
    }

    if (!this.state.valid) {
      this.setState({
        error: null,
        hasSubmitted: true,
        submitting: false,
      });
      return;
    }

    this.setState({
      hasSubmitted: true,
      submitting: true,
    });

    try {
      await this.props.onSubmit(this.submitValues);
    } catch (error) {
      console.error(error);
    }
  };

  handleDatePickerStartFocusChange = ({ focused }) => {
    this.setState({
      datePickerStartFocused: focused,
    });
  };

  handleDatePickerEndFocusChange = ({ focused }) => {
    this.setState({
      datePickerEndFocused: focused,
    });
  };

  render() {
    return (
      <Modal open={this.props.open} onClose={this.props.onClose}>
        <Modal.Header>Filter Repairs</Modal.Header>
        <Modal.Content>
          <Form loading={this.state.submitting} error={!this.state.valid || !!this.state.error}>
            <Segment vertical>
              <h4>Date from:</h4>

              <Form.Group>
                <Form.Field inline error={this.state.hasSubmitted && !!this.state.errors.dateStart}>
                  <SingleDatePicker
                    isOutsideRange={alwaysFalse}
                    placeholder="MM/DD/YYYY"
                    date={this.state.values.dateStart}
                    onDateChange={this.handleDateStartChange}
                    focused={this.state.datePickerStartFocused}
                    onFocusChange={this.handleDatePickerStartFocusChange}
                    numberOfMonths={1}
                    showClearDate
                    keepOpenOnDateSelect
                  />
                </Form.Field>

                <Form.Dropdown
                  error={this.state.hasSubmitted && !!this.state.errors.hoursStart}
                  scrolling
                  disabled={this.state.values.dateStart == null}
                  label="Hours"
                  item
                  placeholder="-"
                  options={HOURS_OPTIONS}
                  onChange={this.handleHoursStartChange}
                  value={this.state.values.hoursStart}
                />

                <Form.Dropdown
                  error={this.state.hasSubmitted && !!this.state.errors.minutesStart}
                  scrolling
                  disabled={
                    this.state.values.dateStart == null || this.state.values.hoursStart == null
                  }
                  label="Minutes"
                  item
                  placeholder="-"
                  options={MINUTES_OPTIONS}
                  onChange={this.handleMinutesStartChange}
                  value={this.state.values.minutesStart}
                />

                <Form.Dropdown
                  error={this.state.hasSubmitted && !!this.state.errors.amPmStart}
                  disabled={
                    this.state.values.dateStart == null || this.state.values.hoursStart == null
                  }
                  label="AM/PM"
                  item
                  placeholder="-"
                  options={AM_PM_OPTIONS}
                  onChange={this.handleAmPmStartChange}
                  value={this.state.values.amPmStart}
                />
              </Form.Group>
              {this.state.hasSubmitted &&
                this.state.errors.dateStart && (
                  <Message error>{this.state.errors.dateStart}</Message>
                )}

              <h4>Date to:</h4>

              <Form.Group>
                <Form.Field inline error={this.state.hasSubmitted && !!this.state.errors.dateEnd}>
                  <SingleDatePicker
                    isOutsideRange={alwaysFalse}
                    placeholder="MM/DD/YYYY"
                    date={this.state.values.dateEnd}
                    onDateChange={this.handleDateEndChange}
                    focused={this.state.datePickerEndFocused}
                    onFocusChange={this.handleDatePickerEndFocusChange}
                    numberOfMonths={1}
                    showClearDate
                    keepOpenOnDateSelect
                  />
                </Form.Field>

                <Form.Dropdown
                  error={this.state.hasSubmitted && !!this.state.errors.hoursEnd}
                  scrolling
                  disabled={this.state.values.dateEnd == null}
                  label="Hours"
                  item
                  placeholder="-"
                  options={HOURS_OPTIONS}
                  onChange={this.handleHoursEndChange}
                  value={this.state.values.hoursEnd}
                />

                <Form.Dropdown
                  error={this.state.hasSubmitted && !!this.state.errors.minutesEnd}
                  scrolling
                  disabled={this.state.values.dateEnd == null || this.state.values.hoursEnd == null}
                  label="Minutes"
                  item
                  placeholder="-"
                  options={MINUTES_OPTIONS}
                  onChange={this.handleMinutesEndChange}
                  value={this.state.values.minutesEnd}
                />

                <Form.Dropdown
                  error={this.state.hasSubmitted && !!this.state.errors.amPmEnd}
                  disabled={this.state.values.dateEnd == null || this.state.values.hoursEnd == null}
                  label="AM/PM"
                  item
                  placeholder="-"
                  options={AM_PM_OPTIONS}
                  onChange={this.handleAmPmEndChange}
                  value={this.state.values.amPmEnd}
                />
              </Form.Group>
              {this.state.hasSubmitted &&
                this.state.errors.dateEnd && <Message error>{this.state.errors.dateEnd}</Message>}

              {!this.props.hideUserFilter && (
                <div>
                  <Form.Dropdown
                    error={this.state.hasSubmitted && !!this.state.errors.userKey}
                    label="User Assigned"
                    selection
                    search
                    placeholder="No filter"
                    options={this.userOptions}
                    onChange={this.handleUserAssignedChange}
                    value={this.state.values.userKey}
                  />
                  {this.state.hasSubmitted &&
                    this.state.errors.status && <Message error>{this.state.errors.status}</Message>}
                </div>
              )}

              <Form.Dropdown
                label="Status"
                error={this.state.hasSubmitted && !!this.state.errors.status}
                trigger={
                  this.state.values.status != null ? (
                    <Label
                      color={
                        this.state.values.status === RepairStatus.COMPLETE ? 'green' : 'orange'
                      }
                    >
                      {this.state.values.status}
                    </Label>
                  ) : (
                    'No filter'
                  )
                }
                item
                placeholder="No filter"
                options={STATUS_OPTIONS}
                onChange={this.handleStatusChange}
                value={this.state.values.status}
              />
              {this.state.hasSubmitted &&
                this.state.errors.status && <Message error>{this.state.errors.status}</Message>}

              {this.state.error && <Message error>{this.state.error}</Message>}
            </Segment>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button onClick={this.handleClearFiltersClick}>Clear Filters</Button>
          <Button
            primary
            onClick={this.handleSubmit}
            disabled={this.state.submitting}
            loading={this.state.submitting}
          >
            Apply Filters
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}
