import React from 'react';
import PropTypes from 'prop-types';
import { Card } from 'semantic-ui-react';
import moment from 'moment';

import { UserShape } from '../shapes';

export default class Comment extends React.Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    date: PropTypes.number.isRequired,
    user: UserShape,
  };

  static defaultProps = {
    user: null,
  };

  render() {
    const { text, date, user } = this.props;
    return (
      <Card
        fluid
        description={text}
        extra={
          <p>
            {moment(date).format('MM/DD/YYYY HH:mm:ss')} by{' '}
            {user && user.email ? user.email : '<deleted User>'}
          </p>
        }
      />
    );
  }
}
