import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Form, Header, Message, Segment } from 'semantic-ui-react';

import firebase from '../firebase';
import { CenterFormLayout } from '../Layout';
import './SignupPage.css';

class SignupForm extends React.Component {
  constructor(props) {
    super(props);
    const state = {
      submitting: false,
      hasSubmitted: false,
      values: {
        email: null,
        password: null,
        passwordRepeat: null,
      },
      valid: true,
      error: null,
      errors: {},
    };

    const errors = this.validate(state.values);
    this.state = {
      ...state,
      errors,
      valid: Object.keys(errors).length === 0,
    };
  }

  handleFieldChange = (key, value) => {
    const newValues = {
      ...this.state.values,
      [key]: value,
    };

    const validationResult = this.validate(newValues);
    const valid = Object.keys(validationResult).length === 0;

    this.setState({
      values: newValues,
      errors: validationResult || {},
      valid,
    });
  };

  validate = (values) => {
    const errors = {};
    if (!values.email || values.email.length === '') {
      errors.email = 'Is required';
    }
    if (!values.password || values.password.length === '') {
      errors.password = 'Is required';
    }
    if (values.password && values.password.length < 6) {
      errors.password = 'The password should be at least 6 characters long';
    }
    if (!values.passwordRepeat || values.passwordRepeat.length === '') {
      errors.passwordRepeat = 'Is required';
    }
    if (values.password !== values.passwordRepeat) {
      errors.passwordRepeat = "Passwords don't match";
    }
    return errors;
  };

  handleEmailChange = event => this.handleFieldChange('email', event.target.value);
  handlePasswordChange = event => this.handleFieldChange('password', event.target.value);
  handlePasswordRepeatChange = event =>
    this.handleFieldChange('passwordRepeat', event.target.value);

  handleSubmit = async () => {
    if (!this.state.valid) {
      this.setState({
        error: null,
        hasSubmitted: true,
      });
      return;
    }

    this.setState({
      hasSubmitted: true,
      submitting: true,
    });

    try {
      const { email, password } = this.state.values;
      await firebase.auth().createUserWithEmailAndPassword(email, password);
    } catch (error) {
      console.error(error);
      this.setState({
        error: error.message || 'Error while signing in',
        submitting: false,
      });
    }
  };

  render() {
    console.log(this.state, this.state.hasSubmitted && this.state.errors.email);
    return (
      <CenterFormLayout>
        <Header as="h2" color="teal" textAlign="center">
          {' '}
          Create an account
        </Header>
        <Form
          size="large"
          loading={this.state.submitting}
          error={!this.state.valid || !!this.state.error}
        >
          <Segment stacked>
            <Form.Field error={this.state.hasSubmitted && this.state.errors.email}>
              <Form.Input
                fluid
                icon="at"
                iconPosition="left"
                placeholder="E-mail address"
                onChange={this.handleEmailChange}
                type="email"
              />
            </Form.Field>
            {this.state.hasSubmitted &&
              this.state.errors.email && <Message error>{this.state.errors.email}</Message>}

            <Form.Field error={this.state.hasSubmitted && this.state.errors.password}>
              <Form.Input
                fluid
                icon="lock"
                iconPosition="left"
                placeholder="Password"
                type="password"
                onChange={this.handlePasswordChange}
              />
            </Form.Field>
            {this.state.hasSubmitted &&
              this.state.errors.password && <Message error>{this.state.errors.password}</Message>}

            <Form.Field error={this.state.hasSubmitted && this.state.errors.passwordRepeat}>
              <Form.Input
                fluid
                icon="lock"
                iconPosition="left"
                placeholder="Repeat Password"
                type="password"
                onChange={this.handlePasswordRepeatChange}
              />
            </Form.Field>
            {this.state.hasSubmitted &&
              this.state.errors.passwordRepeat && (
                <Message error>{this.state.errors.passwordRepeat}</Message>
              )}

            {this.state.error && <Message error>{this.state.error}</Message>}

            <Button color="teal" fluid size="large" onClick={this.handleSubmit}>
              Sign up
            </Button>
          </Segment>
        </Form>
        <Message>
          Already have an account? <Link to="/login">Log-in</Link>
        </Message>
      </CenterFormLayout>
    );
  }
}

export default SignupForm;
