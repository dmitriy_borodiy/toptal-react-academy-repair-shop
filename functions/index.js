const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp(functions.config().firebase);

const express = require('express');
const cookieParser = require('cookie-parser')();
const cors = require('cors')({ origin: true });

// Express middleware that validates Firebase ID Tokens passed in the Authorization HTTP header.
// The Firebase ID token needs to be passed as a Bearer token in the Authorization HTTP header like this:
// `Authorization: Bearer <Firebase ID Token>`.
// when decoded successfully, the ID Token content will be added as `req.user`.
const validateFirebaseIdToken = (req, res, next) => {
  console.log('Check if request is authorized with Firebase ID token');

  if (
    (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) &&
    !req.cookies.__session
  ) {
    console.error('No Firebase ID token was passed as a Bearer token in the Authorization header.');
    res.status(403).send('Unauthorized');
    return;
  }

  let idToken;
  if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
    console.log('Found "Authorization" header');
    // Read the ID Token from the Authorization header.
    idToken = req.headers.authorization.split('Bearer ')[1];
  } else {
    console.log('Found "__session" cookie');
    // Read the ID Token from cookie.
    idToken = req.cookies.__session;
  }
  admin
    .auth()
    .verifyIdToken(idToken)
    .then((decodedIdToken) => {
      console.log('ID Token correctly decoded', decodedIdToken);
      req.user = decodedIdToken;
      next();
    })
    .catch((error) => {
      console.error('Error while verifying Firebase ID token:', error);
      res.status(403).send('Unauthorized');
    });
};

const app = express();
app.use(cors);
app.use(cookieParser);
app.use(validateFirebaseIdToken);

function combineMiddleware(mids) {
  return mids.reduce((a, b) =>
    (req, res, next) => {
      a(req, res, (err) => {
        if (err) {
          return next(err);
        }
        return b(req, res, next);
      });
    }
  );
}

const middleware = combineMiddleware([cors, cookieParser, validateFirebaseIdToken]);

exports.onUserCreate = functions.auth.user().onCreate((event) => {
  const userId = event.data.uid;
  return admin
    .database()
    .ref(`/users/${userId}`)
    .set({
      key: userId,
      email: event.data.email,
      role: 'user', // default role: user
    });
});

exports.onUserDelete = functions.auth.user().onDelete((event) => {
  const userId = event.previous.data.uid || event.data.uid;
  return admin
    .database()
    .ref(`/users/${userId}`)
    .remove();
});

exports.onUserObjectDelete = functions.database.ref('/users/{userId}').onDelete((event) => {
  const userId = event.params.userId;
  return admin.auth().deleteUser(userId);
});

const ONE_HOUR = 1000 * 60 * 60; // 1000ms * 60s * 60m = 1 hour

exports.updateRepair = functions.https.onRequest((req, res) => {
  return middleware(req, res, () => {
    const user = req.user;

    return admin.database().ref(`/users/${user.uid}/role`).once('value', (userRole) => {
      if (userRole.val() !== 'manager') {
        return res.status(403).send('Unauthorized to update Repair');
      }

      const { key, userKey, time, comments, status, approved } = req.body;

      return admin
        .database()
        .ref('/repairs/')
        .transaction(
          (data) => {
            console.log('data', data);
            const dataOrEmpty = data || {};
            const times = Object.keys(dataOrEmpty).filter((repairKey) => {
              if (!key) {
                return true;
              }
              return key !== repairKey;
            }).map(k => dataOrEmpty[k].time);

            const overlappingTime = times.find(t => Math.abs(time - t) < ONE_HOUR);
            if (overlappingTime) {
              console.log('overlapping time');
              res.status(400).send('Repair times overlap');
              throw new Error('Repair times overlap');
            }

            const oldDataForKey = dataOrEmpty[key] || {};
            return Object.assign({}, dataOrEmpty, {
              [key]: Object.assign({}, oldDataForKey, {
                key,
                userKey: userKey || null,
                time: time || new Date().getTime(),
                comments: comments || [],
                status: status || 'incomplete',
                approved: approved || false,
              }),
            });
          },
          (error) => {
            console.log('callback');
            if (!error) {
              return res.status(200).send('OK');
            }
          }
        ).then((success) => {
          console.log('then', success);
        }).catch((error) => {
          console.error(error);
          return res.status(500).send('Server error');
        });
    });
  });
});
